FROM java:8
EXPOSE 3001
ADD /target/service1.jar service1.jar
ENTRYPOINT ["java","-jar","service1.jar"]