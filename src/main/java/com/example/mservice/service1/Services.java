package com.example.mservice.service1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Services {

	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping(value = "/hello")
	public String getMessage() {
		
		String service2Msg = "";
		
		ResponseEntity<String> c = restTemplate.exchange("http://MICRO-SERVICE2/hello", HttpMethod.GET, null, new ParameterizedTypeReference<String>(){});
		service2Msg = c.getBody();
		
		System.out.println(service2Msg);
		
		return "Hello from micro-service1. Call from MICRO-SERVICE1 to MICRO-SERVICE2 is 200(ok) with message: " + service2Msg;
	}
}
